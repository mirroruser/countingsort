import java.util.Arrays;

public class CountingSort {
    public static void countingSort (int[] arr) {
        int max = arr[0];
        int min = arr[0];

        for (int i = 0; i < arr.length; i++) {
            max = arr[i] > max ? arr[i] : max;
            min = arr[i] < min ? arr[i] : min;
        }

        int[] count = new int[max - min + 1];
        for (int i = 0; i < arr.length; i++)
            count[arr[i] - min]++;

        for (int i = 1; i < count.length; i++)
            count[i] = count[i] + count[i - 1];

        int[] output = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            count[arr[i] - min]--;
            output[count[arr[i] - min]] = arr[i];
        }

        for (int i = 0; i < output.length; i++)
            arr[i] = output[i];
    }

    public static void main (String[] args) {
        int n = (int) (Math.random() * 10000);
        n = 10000;
        int[] test = new int[n];

        long t0;

        for (int i = 0; i < test.length; i++) {
            test[i] = (int) (Math.random() * n + 1);
            if (i % 4 == 0) test[i] = -1 * test[i];
        }

        System.out.println(Arrays.toString(test));
        t0 = Profiler.tick();
        CountingSort.countingSort(test);
        System.out.println("Operation took approx " + Profiler.diff(t0) + "ns");
        System.out.println(Arrays.toString(test));
    }
}
